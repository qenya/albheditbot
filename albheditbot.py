#!/usr/bin/env python3

import json
import os
import re
from uuid import uuid4

import redis
from tornado.httpclient import HTTPClient, HTTPRequest
from tornado.ioloop import IOLoop
from tornado.web import Application, RequestHandler


cipher = str.maketrans(
    'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
    'ypltavkrezgmshubxncdijfqowYPLTAVKREZGMSHUBXNCDIJFQOW'
)

r = redis.from_url(os.environ['REDIS_URL'])


def encipher(text):
    """Converts a text into the Al Bhed cipher. Only works on ASCII letters,
    does not account for accents etc. Substrings within square brackets []
    are not enciphered."""

    bits = re.split(r'\[([^\]]*)\]', text)
    translated = [x.translate(cipher) if i%2==0 else x for i,x in enumerate(bits)]
    return ''.join(translated)


def handle_inline_query(inline_query):
    if len(inline_query['query']) == 0:
        # Telegram continuously polls us for the result while the user is
        # typing, including when they haven't typed anything yet. But it
        # doesn't let us return an empty string as a response.
        # So, check if the query is empty and return nothing if so.
        return {
            'method': 'answerInlineQuery',
            'inline_query_id': inline_query['id'],
            'results': []
        }
    else:
        text = inline_query['query']
        result = encipher(text)

        # Generate a GUID for this result so we can store it and read it
        # back later when the "Show" button is pressed.
        # For privacy, we only keep messages for 28 days, then people have to
        # translate manually
        guid = str(uuid4())
        r.set(guid, text, 2419200)

        return {
            'method': 'answerInlineQuery',
            'inline_query_id': inline_query['id'],
            'results': [{
                'type': 'article',
                'id': inline_query['id'],
                'title': 'Send',
                'input_message_content': {'message_text': result},
                'reply_markup': {
                    'inline_keyboard': [[{
                        'text': 'Show',
                        'callback_data': guid
                    }]]
                },
                'description': result
            }]
        }


def handle_callback_query(callback_query):
    if 'data' in callback_query:
        text = r.get(callback_query['data']);

        if text:
            output = text.decode('utf-8')
        else:
            output = 'Message expired. Try converting with a tool like https://stephenw32768.appspot.com/albhed/'
    else:
        output = 'Unexpected error'

    # Telegram enforces a maximum limit of 200 characters on callback query
    # responses, so if the message is longer we have to truncate it, ugh.
    # At least the message can still be decoded manually.
    # NB: Testing suggests the limit is actually 256 characters, but let's
    # stick to what it says in the documentation to be safe.
    if len(output) > 200:
        output = output[:199] + '…'

    return {
        'method': 'answerCallbackQuery',
        'callback_query_id': callback_query['id'],
        'text': output,
        'show_alert': True
    }


class WebhookHandler(RequestHandler):
    def post(self):
        update = json.loads(self.request.body)

        if 'inline_query' in update:
            response = handle_inline_query(update['inline_query'])
        elif 'callback_query' in update:
            response = handle_callback_query(update['callback_query'])
        
        self.set_header('Content-Type', 'application/json')
        self.write(json.dumps(response));


def register_webhook():
    """Make a request to the Telegram API server to register our webhook"""

    http_client = HTTPClient()
    http_request = HTTPRequest(
        url='https://api.telegram.org/bot' + os.environ['BOT_TOKEN'] + '/setWebhook',
        method='POST',
        headers={'Content-Type': 'application/json'},
        body=json.dumps({
            'url': os.environ['HEROKU_URL'] + os.environ['BOT_TOKEN'],
            'allowed_updates': ['inline_query', 'callback_query']
        })
    )

    try:
        response = http_client.fetch(http_request)
    except Exception as e:
        print("Error while registering webhook: " + str(e))
        exit(1)

    result = json.loads(response.body.decode('utf-8'))
    if not result:
        print("Failed to register webhook")
        exit(2)

if __name__ == '__main__':
    app = Application([('/' + os.environ['BOT_TOKEN'], WebhookHandler)])
    app.listen(os.environ['PORT'])

    register_webhook()

    IOLoop.current().start()
    
