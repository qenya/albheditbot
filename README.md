# AlBhedItBot

[@AlBhedItBot](https://t.me/AlBhedItBot) is a Telegram bot you can use to convert your messages inline into the Al Bhed cipher from _Final Fantasy X_. This can be useful for, for example, hiding a spoiler in a group chat or sending a message without it being immediately intelligible in a push notification.

Invoke the bot by tagging it:
```
@AlBhedItBot (your message here)
```

The enciphered message will appear with a "Show" button that anyone can click to see the original text. Words within \[square brackets\] will not be affected.

## Deployment

This bot is currently running on Heroku at https://telegram-albhedit.herokuapp.com/. You can run it on your own Heroku dyno using the instructions [here](https://devcenter.heroku.com/articles/git). Make sure to insert your own [Telegram Bot API token](https://core.telegram.org/bots) in the `BOT_TOKEN` environment variable.

## Origin

This bot was inspired by the similar [@Rot13ItBot](https://github.com/CatTrinket/rot13itbot), which has the same purpose but less whimsy.

Because it works similarly to the original, this bot has the same privacy drawback of having to store all messages on its server so that they can be decoded later by users. To help mitigate this, messages are deleted after 28 days and must be decoded manually.
